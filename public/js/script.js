"use strict";
// Servises filter
let servisesMenuItems = document.querySelectorAll(".description");
let servisesMenu = document.querySelector(".description_nav");
let serviceDescriptions = document.querySelectorAll(".description_item");
serviceDescriptions[0].style.cssText = "display: Flex;";
servisesMenu.addEventListener("click", (e) => {
  if (e.target.closest("li")) {
    for (const i of serviceDescriptions) {
      i.style.cssText = "display: none;";
      if (e.target.innerText == i.id) {
        i.style.cssText = "display: flex;";
      }
    }
  }
});

// Filter
let worksList = document.querySelectorAll(".works_item");
let ul = document.querySelector(".works_list");
let worksListArch = [];
let loadMore = [];
let filtr = document.querySelector(".filter");
let filtrItems = document.querySelectorAll(".filter > li");
let loadButton = document.querySelector(".load_more");
console.log(filtrItems);
for (const i of worksList) {
  worksListArch.push(i);
  i.remove();
}
for (let i = 0; i < 12; i++) {
  ul.append(worksListArch[i]);
}

for (let i = 0; i < 12; i++) {
  loadMore.push(worksListArch[1].cloneNode(true));
}
loadMore[0]
  .querySelector("img")
  .setAttribute("src", "./images/web design/web-design1.jpg");
loadMore[1]
  .querySelector("img")
  .setAttribute("src", "./images/web design/web-design2.jpg");
loadMore[2]
  .querySelector("img")
  .setAttribute("src", "./images/web design/web-design3.jpg");
loadMore[3]
  .querySelector("img")
  .setAttribute("src", "./images/landing page/landing-page1.jpg");
loadMore[3].setAttribute("data-type", "Landing Pages");
loadMore[4]
  .querySelector("img")
  .setAttribute("src", "./images/landing page/landing-page2.jpg");
loadMore[4].setAttribute("data-type", "Landing Pages");
loadMore[5]
  .querySelector("img")
  .setAttribute("src", "./images/landing page/landing-page3.jpg");
loadMore[5].setAttribute("data-type", "Landing Pages");
loadMore[6]
  .querySelector("img")
  .setAttribute("src", "./images/web design/web-design4.jpg");
loadMore[6].setAttribute("data-type", "Graphic Design");
loadMore[7]
  .querySelector("img")
  .setAttribute("src", "./images/web design/web-design5.jpg");
loadMore[7].setAttribute("data-type", "Graphic Design");
loadMore[8]
  .querySelector("img")
  .setAttribute("src", "./images/web design/web-design6.jpg");
loadMore[8].setAttribute("data-type", "Graphic Design");
loadMore[9]
  .querySelector("img")
  .setAttribute("src", "./images/wordpress/wordpress1.jpg");
loadMore[9].setAttribute("data-type", "Wordpress");
loadMore[10]
  .querySelector("img")
  .setAttribute("src", "./images/wordpress/wordpress2.jpg");
loadMore[10].setAttribute("data-type", "Wordpress");
loadMore[11]
  .querySelector("img")
  .setAttribute("src", "./images/wordpress/wordpress3.jpg");
loadMore[11].setAttribute("data-type", "Wordpress");

filtr.addEventListener("click", (e) => {
  let worksListNow = document.querySelectorAll(".works_item");
loadButton.style.cssText = "display: flex;"
  if (e.target.closest("li")) {
    for (const i of filtrItems) {
      if (i !== e.target) {
        i.setAttribute("class", "filter_item");
      } else {
        console.log("fff");
        e.target.setAttribute("class", "filter_item_active");
      }
    }
    for (const i of worksListNow) {
      i.remove();
    }
    for (const i of worksListArch) {
      if (i.getAttribute("data-type") === e.target.innerText) {
        ul.append(i);
      }
    }
    if (e.target.innerText === "All") {
      for (const i of worksListArch) {
        ul.append(i);
      }
    }
  }
});
loadButton.addEventListener("click", (e)=>{
  let active = document.querySelector(".filter_item_active");
  for (const i of loadMore) {
    if (active.innerText === i.getAttribute("data-type")) {
      ul.append(i);
    }
  }
  if (active.innerText === "All") {
    for (const i of loadMore) {
      ul.append(i);
    }
  }
  loadButton.style.cssText = "display: none;"
})

// slider JS мой без анимации, оставил для себя
// let allMembers = document.querySelectorAll(".rewiewer");
// let slider = document.querySelector(".slider_cut");
// let Arrows = document.querySelectorAll(".arrow");
// let membersContainer = [];
// let index;
// for (const i of allMembers) {
//   membersContainer.push(i);
//   i.remove();
// }
// for (let i = 0; i < 4; i++) {
//   slider.append(membersContainer[i]);
// }
// Arrows[0].addEventListener("click", () => {
//   let activeMembers = document.querySelectorAll(".rewiewer");
//   for (const i of membersContainer) {
//     if (activeMembers[3] === i) {
//       index = membersContainer.indexOf(i);
//     }
//   }
//   if (index < membersContainer.length - 1) {
//     slider.append(membersContainer[index + 1]);
//     for (const i of activeMembers) {
//       i.style.cssText = "transition: all easy  5s;";
//       i.style.left = -30 + 'px';
//     }
//     activeMembers[0].remove();

//   } else {
//     slider.append(membersContainer[0]);
//     activeMembers[0].remove();
//     index = 0;
//   }
// });
// Arrows[1].addEventListener("click", () => {
//   let activeMembers = document.querySelectorAll(".rewiewer");
//   for (const i of membersContainer) {
//     if (activeMembers[0] === i) {
//       index = membersContainer.indexOf(i);
//     }
//   }
//   if (index > 0) {
//     activeMembers[3].remove();
//     slider.prepend(membersContainer[index - 1]);
//   } else {
//     activeMembers[3].remove();
//     slider.prepend(membersContainer[activeMembers.length]);
//     index = activeMembers.length;
//   }
// });
var galleryTop = new Swiper(".gallery-top", {
  spaceBetween: 10,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  loop: true,
  loopedSlides: 9,
});
var galleryThumbs = new Swiper(".gallery-thumbs", {
  spaceBetween: 10,
  centeredSlides: true,
  
  touchRatio: 0.2,
  slideToClickedSlide: true,
  loop: true,
  loopedSlides: 9,
  slidesPerView: 4,
});
galleryTop.controller.control = galleryThumbs;
galleryThumbs.controller.control = galleryTop;

